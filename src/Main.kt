fun main() {
    val points = arrayOf(
            Point2D(0.0, 0.0),
            Point2D(10.0, 10.0)
    )
    val materialPoints = arrayOf(
            MaterialPoint2D(0.0, 0.0, 10.0),
            MaterialPoint2D(10.0, 10.0, 100.0)
    )

    val geometricCenter = Calculations.positionGeometricCenter(points)
    val massCenter = Calculations.positionCenterOfMass(materialPoints)

    println("Polozenie srodka masy: $massCenter")
    println("Polozenie srodka geometrycznego: $geometricCenter")
}