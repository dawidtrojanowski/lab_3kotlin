open class Point2D(
        val x: Double, val y: Double
) {

    override fun toString(): String {
        return "Point2D{" +
                "x=" + x +
                ", y=" + y +
                '}'.toString()
    }
}
